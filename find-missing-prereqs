#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2023 Google LLC

import argparse

from stable_utils import *

parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=
"""Finds patches that were backported without previous patches in their original
series also being backported.  For each series containing such a patch, lists
the missing previous patches.

This command requires a commit range.  Normally it should be something like
stable/linux-6.1.y..HEAD, where HEAD includes the pending patches for 6.1.
""")
parser.add_argument('commit_range')
parser.add_argument('--autosel-only', action='store_true',
                    help="only consider AUTOSEL'd commits")
args = parse_args(parser)

for (patches, backports, missing) \
        in find_missing_prereqs(args.commit_range, args.autosel_only):
    print('The following commit(s):')
    for (i, commit) in backports:
        prefix = ' AUTOSEL' if commit.is_autosel() else ''
        print(f'  [PATCH{prefix} {i}/{len(patches)-1}] {commit}')
    print('... are backported without earlier commit(s) in series:')
    for (i, commit) in missing:
        print(f'  [PATCH {i}/{len(patches)-1}] {commit}')
    cover_letter = patches[0]
    if cover_letter:
        subject = cover_letter["Subject"].replace('\n', ' ')
        print(f'Original patch series is "{subject}"')
        print(f'({config.lore}/r/{get_message_id(cover_letter)})')
    print('')
