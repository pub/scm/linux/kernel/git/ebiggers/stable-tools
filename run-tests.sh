#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2023 Google LLC

set -e -u

cd "$(dirname "$(realpath "$0")")"
export PATH="$PWD:$PATH"

for script in tests/*.py; do
	echo "Running $script"
	if ! PYTHONPATH=. python3 "$script"; then
		echo 1>&2 "$script failed!"
		exit 1
	fi
done
echo "All tests passed!"
