# Introduction

This repository contains some scripts that may be helpful for maintenance of
stable and Long Term Support branches of the Linux kernel.

# Prerequisites

Install the following packages:

- `python`
- `python-requests`
- `python-beautifulsoup4`

# List of scripts

- `find-orig-patch`: Finds the original patch email from a git commit.

- `find-orig-series`: Finds the original patch series from a git commit.

- `find-missing-prereqs`: Finds patches that were backported without previous
  patches in their original series also being backported.

For more information, run each script with `--help`.

If anything goes wrong, using the `--verbose` option might help diagnose it.
There are also tests, which can be run using `run-tests.sh`.

# Configuration

Settings such as the location of the Linux git repo are defined by the `Config`
in `stable_utils.py`.  These settings can be overridden by creating a file
`config.py`, in this directory, that contains a method `customize(config)`.  For
example, your `config.py` could contain:

```
import os

def customize(config):
    config.linux_dir = os.getenv('HOME') + '/src/linux'
```

# Information sources

These scripts use two sources of information: a local Linux git repo and a
remote mailing list archive server.

## Linux git repo

`config.linux_dir` must be a clone of Linux that contains:

- Any git commit IDs that are explicitly specified in commands

- A git ref `config.upstream` (default: `origin/master`) that refers to the
  latest mainline.  This is used to check whether commits are "upstream".

- The history going back at least to `config.start_of_history` (default: `v4.0`)

## lore.kernel.org

`config.lore` (default: `https://lore.kernel.org`) must be a
[public-inbox](https://public-inbox.org/README.html) server that archives the
mailing lists that the patches for the git commits are sent to.  The scripts
query this server to find patches.

These scripts make a large number of queries, so please don't run these scripts
excessively.  However, all responses are cached for `config.lore_cache_timeout`
(default: 24 hours) in the `lore_cache` directory.  So, redundant requests won't
be made if e.g. a script is run multiple times with the same arguments.
