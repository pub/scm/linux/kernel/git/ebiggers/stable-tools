# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2023 Google LLC

from stable_utils import *

TEST_CASES = [
    {
        'commit_range': '81c1188905f88b77743d1fdeeedfc8cb7b67787d~3..81c1188905f88b77743d1fdeeedfc8cb7b67787d',
        'results': [
            {
                # [PATCH v4 0/5] blk-iocost: random bugfix
                'cover_letter': '20230117070806.3857142-1-yukuai1@huaweicloud.com',
                'backports': [
                    (4, 'bf8eb1fd6110871e6232e8e7efe399276ef7e6f6'),
                ],
                'missing': [
                    (1, '7b7c5ae4402f810612e84c4ded7a302a61eeba59'),
                    (2, '235a5a83f637e32646cd004c9b580decf1225566'),
                    (3, '35198e32300190d52e7f50413dde4f86dea1de92'),
                ],
            },
            {
                # [PATCH -next v3 0/3] blk-cgroup: make sure pd_free_fn() is called in order
                'cover_letter': '20230119110350.2287325-1-yukuai1@huaweicloud.com',
                'backports': [
                    (1, '029f1f1efa84387474b445dac4281cf95a398db8'),
                    (3, '81c1188905f88b77743d1fdeeedfc8cb7b67787d'),
                ],
                'missing': [
                    (2, 'dfd6200a095440b663099d8d42f1efb0175a1ce3'),
                ],
            },
        ],
    }, {
        'commit_range': 'a6f81fcb2c3905c28641837dc823ed34617eb110~1..a6f81fcb2c3905c28641837dc823ed34617eb110',
        'results': [
            {
                # [PATCH net-next 0/3] tcp: better receiver autotuning
                'cover_letter': '20171211015504.26551-1-edumazet@google.com',
                'backports': [
                    (2, 'a6f81fcb2c3905c28641837dc823ed34617eb110'),
                ],
                'missing': [
                    (1, '02db55718d53f9d426cee504c27fb768e9ed4ffe'),
                ],
            },
        ],
    },
]

def assert_equals(expected, actual):
    if expected != actual:
        raise AssertionError(f"Expected '{expected}' but was '{actual}'")

for case in TEST_CASES:
    for (i, (patches, backports, missing)) \
            in enumerate(find_missing_prereqs(case['commit_range'], False)):
        expected_result = case['results'][i]
        assert_equals(expected_result['cover_letter'], get_message_id(patches[0]))
        assert_equals(expected_result['backports'],
                      [(i, commit.id) for (i, commit) in backports])
        assert_equals(expected_result['missing'],
                      [(i, commit.id) for (i, commit) in missing])
