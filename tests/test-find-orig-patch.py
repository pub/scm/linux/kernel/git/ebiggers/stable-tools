# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2023 Google LLC

from stable_utils import *

TEST_CASES = [
    # A commit that contains a "Message-Id:" reference to the patch
    {
        'commit_title': 'vhost_vdpa: fix unmap process in no-batch mode',
        'commit_id': 'c82729e06644f4e087f5ff0f91b8fb15e03b8890',
        'message_id': '20230420151734.860168-1-lulu@redhat.com',
        'patch_subject': '[PATCH v3] vhost_vdpa: fix unmap process in no-batch mode',
    }, {
        # A commit whose message contains a "Link:" to the patch on lore.kernel.org
        'commit_title': 'fsverity: reject FS_IOC_ENABLE_VERITY on mode 3 fds',
        'commit_id': '04839139213cf60d4c5fc792214a08830e294ff8',
        'message_id': '20230406215106.235829-1-ebiggers@kernel.org',
        'patch_subject': '[PATCH] fsverity: reject FS_IOC_ENABLE_VERITY on mode 3 fds',
    }, {
        # A commit whose message contains a "Link:" to the patch on lkml.kernel.org
        'commit_title': 'libgcc: add forward declarations for generic library routines',
        'commit_id': '4f20b7471c57032860065591a17efd3325216bde',
        'message_id': '5cdbe08296693dd53849f199c3933e16e97b33c1.1682088593.git.geert+renesas@glider.be',
        'patch_subject': '[PATCH] libgcc: Add forward declarations for generic library routines',
    }, {
        # A commit whose message contains a "Link:" to the patch on a patchwork server
        'commit_title': 'drm/bridge: tc358767: Set default CLRSIPO count',
        'commit_id': '01338bb82fed40a6a234c2b36a92367c8671adf0',
        'message_id': '20221016003556.406441-1-marex@denx.de',
        'patch_subject': '[PATCH] drm/bridge: tc358767: Set default CLRSIPO count',
    }, {
        # A commit whose message doesn't link to the original patch in any way
        'commit_title': 'block/keyslot-manager: prevent crash when num_slots=1',
        'commit_id': '47a846536e1bf62626f1c0d8488f3718ce5f8296',
        'message_id': '20201111214855.428044-1-ebiggers@kernel.org',
        'patch_subject': '[PATCH v2] block/keyslot-manager: prevent crash when num_slots=1',
    }, {
        # A commit whose message contains a "Link:" to lore.kernel.org, but it
        # goes to a bug report instead of to the patch
        'commit_title': 'btrfs: set default discard iops_limit to 1000',
        'commit_id': 'e9f59429b87d35cf23ae9ca19629bd686a1c0304',
        'message_id': '73afd4f8e96351ffe1ba0d50d4fca75873ae3c54.1680723651.git.boris@bur.io',
        'patch_subject': '[PATCH v2 1/2] btrfs: set default discard iops_limit to 1000',
    }, {
        # A commit whose message doesn't link to the original patch in any way,
        # and which was later resent as a backport with an unusually-formatted
        # subject
        # (https://lore.kernel.org/r/1527712565-25686-1-git-send-email-linux@roeck-us.net).
        'commit_title': 'tcp: avoid integer overflows in tcp_rcv_space_adjust()',
        'commit_id': 'a6f81fcb2c3905c28641837dc823ed34617eb110',
        'message_id': '20171211015504.26551-3-edumazet@google.com',
        'patch_subject': '[PATCH net-next 2/3] tcp: avoid integer overflows in tcp_rcv_space_adjust()',
    }, {
        # A commit whose message doesn't link to the original patch in any way,
        # and which was later resent as a backport without the kernel version
        # being mentioned in the subject
        # (https://lore.kernel.org/r/1529300228-7457-1-git-send-email-linux@roeck-us.net).
        'commit_title': 'tcp: do not overshoot window_clamp in tcp_rcv_space_adjust()',
        'commit_id': '02db55718d53f9d426cee504c27fb768e9ed4ffe',
        'message_id': '20171211015504.26551-2-edumazet@google.com',
        'patch_subject': '[PATCH net-next 1/3] tcp: do not overshoot window_clamp in tcp_rcv_space_adjust()',
    },
]

def assert_equals(expected, actual):
    if expected != actual:
        raise AssertionError(f"Expected '{expected}' but was '{actual}'")

for case in TEST_CASES:
    commit = Commit(case['commit_id'])
    assert_equals(case['commit_title'], commit.get_title())
    message_id = commit.find_original_email()
    assert_equals(case['message_id'], message_id)
    message = fetch_message(message_id)
    assert_equals(case['patch_subject'], message['Subject'])
