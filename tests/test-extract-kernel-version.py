# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2023 Google LLC

from stable_utils import *

TEST_CASES = [
    ('v6.1', 'v6.1'),
    ('v6.1.23', 'v6.1'),
    ('v6.3-rc7', 'v6.2'),
    ('v6.3', 'v6.3'),
    ('v6.4-rc1', 'v6.3'),
]

def assert_equals(expected, actual):
    if expected != actual:
        raise AssertionError(f"Expected '{expected}' but was '{actual}'")

for case in TEST_CASES:
    assert_equals(case[1], extract_kernel_version(case[0]))
